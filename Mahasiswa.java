import java.util.Scanner;

class Mahasiswa {
    String nama;
    String jurusan;
    int nilai_fisika;
    int nilai_kimia;
    int nilai_biologi;

    Mahasiswa () {}
    Mahasiswa (String nama, String jurusan) {
        this.nama = nama;
        this.jurusan = jurusan;
    }
    Mahasiswa (String nama, String jurusan, int nilai_fisika, int nilai_kimia, int nilai_biologi) {
        this.nama = nama;
        this.jurusan = jurusan;
        this.nilai_fisika = nilai_fisika;
        this.nilai_kimia = nilai_kimia;
        this.nilai_biologi = nilai_biologi;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        do {
            System.out.println("MENU");
            System.out.println("1. Konstruktor tanpa parameter");
            System.out.println("2. Konstruktor dengan 2 parameter");
            System.out.println("3. Konstruktor dengan 5 parameter");
            System.out.println("4. EXIT");
            System.out.print("Pilih operator (1/2/3/4): ");
            int pilihan = input.nextInt();

           if (pilihan==1) {
               Mahasiswa mahasiswa = new Mahasiswa();
           } else if (pilihan ==2) {
               Mahasiswa mahasiswa2 = new Mahasiswa("Steve", "IT");
               System.out.println(mahasiswa2.nama+" "+mahasiswa2.jurusan);
           } else if (pilihan==3) {
               Mahasiswa mahasiswa3 = new Mahasiswa("Steve", "IT", 7,8,9);
               System.out.println(mahasiswa3.nama+" "+mahasiswa3.jurusan);
               System.out.println(mahasiswa3.nilai_fisika + " " + mahasiswa3.nilai_kimia + " " + mahasiswa3.nilai_biologi);
           } else if (pilihan==4){
               break;
           } else {
               System.out.println("Menu yang anda masukkan tidak valid");
           }
        } while (true);










    }




}
