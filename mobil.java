class mobil {
    static String merek;
    static String warna;
    boolean statusBerjalan;

    mobil(String merek,String warna, boolean statusBerjalan){
        this.merek=merek;
        this.warna =warna;
        this.statusBerjalan=statusBerjalan;
    }

    void info() {
        System.out.println("Merek mobil = " + merek);
        System.out.println("Warna mobil = " + warna);
    }

    void berjalan() {
        statusBerjalan = true;
        System.out.println(merek+ " warna " + warna + " sedang berjalan");
    }

    void berhenti() {
        statusBerjalan = false;
        System.out.println(merek+ " warna " + warna + " sedang berhenti");
    }



}
