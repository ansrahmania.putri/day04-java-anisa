import java.util.Scanner;

public class Volume {
    public static int volBangun (int panjang, int lebar, int tinggi) {
        return panjang * lebar * tinggi;
    }

    public static double volBangun (double pi, double r) {
        return 1.33333 * pi * r * r * r;
    }

    public static double volBangun (double pi, double r, double tinggi) {
        return pi * r * r * tinggi;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        do {
            System.out.println("Menu");
            System.out.println("1. Volume balok");
            System.out.println("2. Volume bola");
            System.out.println("3. Volume tabung");
            System.out.println("4. EXIT");
            System.out.println("Pilih operator (1/2/3/4)");
            int pilihan = input.nextInt();

            if (pilihan==1) {
                System.out.println("Perhitungan Volume Balok");
                System.out.print("Panjang balok : ");
                int panjang = input.nextInt();
                System.out.print("Lebar balok : ");
                int lebar = input.nextInt();
                System.out.print("Tinggi balok :");
                int tinggi = input.nextInt();

                System.out.print("Volume balok adalah :" + volBangun(panjang,lebar,tinggi)+"\n");
            } else if (pilihan==2) {
                System.out.println("\nPerhitungan Volume Bola");
                System.out.print("Pi :");
                double piBola = input.nextDouble();
                System.out.print("Jari-jari bola :");
                double rBola = input.nextDouble();

                System.out.println("Volume bola adalah :" + volBangun(piBola,rBola));
            } else if (pilihan==3) {
                System.out.print("\nPerhitungan Volume Tabung : ");
                System.out.print("Pi :");
                double piTabung = input.nextDouble();
                System.out.print("Jari-jari tabung :");
                double rTabung = input.nextDouble();
                System.out.print("Tinggi tabung :");
                double tinggiTabung = input.nextDouble();

                System.out.println("Volume tabung adalah :" + volBangun(piTabung,rTabung,tinggiTabung));
            } else if (pilihan==4){
                break;
            } else {
                System.out.println("Angka yang anda masukkan tidak valid");
            }
        } while(true);
    }





}
