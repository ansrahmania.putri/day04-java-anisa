import java.util.Scanner;

public class kalkulatorSederhana {
    public static int penjumlahan(int x, int y) {
        return x + y;
    }
    public static int pengurangan(int x, int y) {
        return x - y;
    }
    public static int perkalian(int x, int y) {
        return x * y;
    }
    public static int pembagian(int x, int y) {
        return x / y;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Menu");
        System.out.println("1. Penjumlahan");
        System.out.println("2. Pengurangan");
        System.out.println("3. Perkalian");
        System.out.println("4. Pembagian");
        System.out.print("Pilih operator (1/2/3/4): ");
        int pilihan = input.nextInt();

        int result = 0;

        if (pilihan > 0 && pilihan <= 4) {

            System.out.print("Masukkan angka pertama : ");
            int angkaPertama = input.nextInt();
            System.out.print("Masukkan angka kedua : ");
            int angkaKedua = input.nextInt();

            switch (pilihan) {
                case 1:
                    result = penjumlahan(angkaPertama, angkaKedua);
                    break;
                case 2:
                    result = pengurangan(angkaPertama, angkaKedua);
                    break;
                case 3:
                    result = perkalian(angkaPertama, angkaKedua);
                    break;
                case 4:
                    result = pembagian(angkaPertama, angkaKedua);
                    break;
                default:
                    System.out.println("Operator yang anda pilih tidak tersedia");
                }
                System.out.println("Hasil nya adalah " + result);
            } else {
                System.out.println("Operator yang anda pilih tidak tersedia");
            }

        }
    }

