import java.util.Scanner;

public class assignment5d4 {
    static Scanner input = new Scanner(System.in);
    static int pilihan;
    static String data;

    public static void dataMahasiswa() {
        System.out.println("Input nama: ");
        String inputNama = input.next();
        System.out.println("Input nilai fisika: ");
        String inputFisika = input.next();
        System.out.println("Input nilai kimia: ");
        String inputKimia = input.next();
        System.out.println("Input nilai biologi: ");
        String inputBiologi = input.next();

        data = inputNama+ "," + inputFisika + "," + inputKimia + "," + inputBiologi;

        System.out.println(data);
    }
    public static void splitData () {
        System.out.println("Parsing dan Print Data");
        String [] parsingData = data.split(",");
        System.out.println(parsingData[0]);
        for(int i=1; i< parsingData.length; i++){
            System.out.print(parsingData[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {

        do {
            System.out.println("MENU");
            System.out.println("1. Input data mahasiswa");
            System.out.println("2. Parsing and printing data");
            System.out.println("3. EXIT");
            System.out.println("Pilih operator (1/2/3)");
            pilihan = input.nextInt();

            if (pilihan==1) {
                dataMahasiswa();
            } else if (pilihan==2) {
                splitData();
            } else if (pilihan==3) {
                break;
            } else {
                System.out.println("Menu yang anda pilih tidak tersedia");
            }
        }  while (true);
    }
}

